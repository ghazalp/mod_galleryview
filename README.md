Un module Gallery pour Joomla 2.5 : mod_galleryview
===============

A faire 
- associer label et description aux fichiers langues .ini

Exemple:
dans mod_galleryview/mod_galleryview.xml
<pre>&lt;field name=&quot;loadjq&quot; 
type=&quot;list&quot; 
default=&quot;1&quot; 
label=&quot;GALLERYVIEW_LOAD_JQUERY&quot; 
description=&quot;GALLERYVIEW_DESC_LOAD_JQUERY&quot;&gt;</pre>

label="GALLERYVIEW_LOAD_JQUERY" est associ� � GALLERYVIEW_LOAD_JQUERY="Load jQuery" 

dans 

/mod_galleryview/language/en-GB.mod_galleryview.ini

et

description="GALLERYVIEW_DESC_LOAD_JQUERY" est associ� � 

GALLERYVIEW_DESC_LOAD_JQUERY="Select if jQuery must be loaded"

NB : description affiche un Tooltip

``` html
<script src="site-scripts.js"></script>
```
